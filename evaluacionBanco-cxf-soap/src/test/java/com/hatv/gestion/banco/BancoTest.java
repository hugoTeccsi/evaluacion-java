package com.hatv.gestion.banco;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hatv.gestion.banco.dto.SucursalBancoDTO;
import com.hatv.gestion.banco.serv.IBancoProviderService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class BancoTest {

	@Autowired
	private IBancoProviderService bancoProviderService;

	@Test

	@DisplayName("test listar bancos")
	public void listarBancosTest() {
		List<SucursalBancoDTO> listBanco = bancoProviderService.getSucursalBanco();

		Assertions.assertTrue(!listBanco.isEmpty());
	}

}
