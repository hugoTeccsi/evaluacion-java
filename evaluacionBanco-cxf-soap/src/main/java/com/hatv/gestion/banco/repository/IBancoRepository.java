package com.hatv.gestion.banco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hatv.gestion.banco.dto.SucursalBancoDTO;
import com.hatv.gestion.banco.models.Banco;

public interface IBancoRepository extends JpaRepository<Banco, Long> {

	@Query("SELECT new com.hatv.gestion.banco.dto.SucursalBancoDTO(s.idBanco, o.idSucursal,o.nombre,o.direccion) FROM Banco s JOIN s.sucursales o")
	public List<SucursalBancoDTO> getSucursalBanco();
}
