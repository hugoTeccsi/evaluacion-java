package com.hatv.gestion.banco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluacionBancoCxfSoapApplication  {

	 public static void main(String[] args) {
	        SpringApplication.run(EvaluacionBancoCxfSoapApplication.class, args);
	    }
}
