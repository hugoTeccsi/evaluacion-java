package com.hatv.gestion.banco.serv;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hatv.gestion.banco.dto.SucursalBancoDTO;
import com.hatv.gestion.banco.repository.IBancoRepository;

@Service
public class BancoProviderService implements IBancoProviderService {

	@Autowired
	private IBancoRepository repository;

	@Override
	public List<SucursalBancoDTO> getSucursalBanco() {
		return repository.getSucursalBanco();
	}
	
	
}
