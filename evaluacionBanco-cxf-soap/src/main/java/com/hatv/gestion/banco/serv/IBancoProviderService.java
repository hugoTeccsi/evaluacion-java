package com.hatv.gestion.banco.serv;

import java.util.List;

import com.hatv.gestion.banco.dto.SucursalBancoDTO;

public interface IBancoProviderService {

	public List<SucursalBancoDTO> getSucursalBanco();
}
