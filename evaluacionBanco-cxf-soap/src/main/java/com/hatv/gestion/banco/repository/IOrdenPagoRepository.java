package com.hatv.gestion.banco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hatv.gestion.banco.models.OrdenPago;

public interface IOrdenPagoRepository extends JpaRepository<OrdenPago, Long> {

}
