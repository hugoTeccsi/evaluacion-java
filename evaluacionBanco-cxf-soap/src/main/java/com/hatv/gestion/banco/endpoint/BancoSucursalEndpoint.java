package com.hatv.gestion.banco.endpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.hatv.gestion.banco.dto.SucursalBancoDTO;
import com.hatv.gestion.banco.serv.IBancoProviderService;
import com.hatv.gestion.banco.service.GetSucursalPorBanco;
import com.hatv.gestion.banco.service.GetSucursalPorBancoResponse;
import com.hatv.gestion.banco.service.ObjectFactory;
import com.hatv.gestion.banco.service.SucursalBanco;

import lombok.extern.slf4j.Slf4j;

@Endpoint
@Slf4j
public class BancoSucursalEndpoint {

	public static final String NAMESPACE_URI = "http://service.banco.gestion.hatv.com/";

	private IBancoProviderService bancoProvService;

	public BancoSucursalEndpoint() {
	}

	@Autowired
	public BancoSucursalEndpoint(IBancoProviderService bancoProvService) {
		this.bancoProvService = bancoProvService;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getSucursalPorBanco")
	@ResponsePayload
	public JAXBElement<GetSucursalPorBancoResponse> getSucursalBanco(@RequestPayload JAXBElement<GetSucursalPorBanco> request) {
		log.info("BancoSucursalEndpoint.getSucursalesPorBanco.inicio");
		log.info("BancoSucursalEndpoint.getSucursalesPorBanco.request idBanco --> {}", request.getValue().getIdBanco());
		ObjectFactory factory = new ObjectFactory();
		GetSucursalPorBancoResponse response = factory.createGetSucursalPorBancoResponse();
		List<SucursalBancoDTO> lista = bancoProvService.getSucursalBanco().stream()
				.filter(p -> p.getIdBanco().compareTo(request.getValue().getIdBanco().longValue()) == 0)
				.collect(Collectors.toList());

		List<SucursalBanco> temp = new ArrayList<>();
		lista.forEach(obj -> {
			SucursalBanco objSucursal = new SucursalBanco();
			objSucursal.setIdBanco(obj.getIdBanco());
			objSucursal.setIdSucursal(obj.getIdSucursal());
			objSucursal.setNombreSucursal(obj.getNombreSucursal());
			objSucursal.setDireccionSucursal(obj.getDireccionSucursal());
			temp.add(objSucursal);
		});
		response.getReturn().addAll(temp);
		log.info("BancoSucursalEndpoint.getSucursalesPorBanco.fin");
		return factory.createGetSucursalPorBancoResponse(response);
	}

}
