package pe.com.hatv;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MoneyParts {

	final static List<BigDecimal> denominacion = Arrays.asList(new BigDecimal(0.05).setScale(2, 1),
			new BigDecimal(0.1).setScale(1, 1), new BigDecimal(0.2).setScale(1, 1), new BigDecimal(0.5).setScale(1, 1),
			new BigDecimal(1).setScale(0), new BigDecimal(2).setScale(0), new BigDecimal(5).setScale(0),
			new BigDecimal(10).setScale(0), new BigDecimal(20).setScale(0), new BigDecimal(50).setScale(0),
			new BigDecimal(100).setScale(0), new BigDecimal(200).setScale(0));

	public static List<List<BigDecimal>> build(String valor) {
		System.out.println("********* Problema 01 **********");
		List<List<BigDecimal>> salida = new ArrayList<>();

		BigDecimal monto = new BigDecimal(valor);
		denominacion.forEach(obj -> {
			List<BigDecimal> temp = new ArrayList<>();
			if (monto.compareTo(obj) >= 0) {
				Double item = monto.divide(obj).doubleValue();
				if (item > 0) {
					if (isEntero(item)) {
						for (int i = 0; i < item.intValue(); i++) {
							temp.add(obj);
						}
					} else {
						Double residuo = monto.doubleValue() % obj.doubleValue();
						if (residuoInList(residuo)) {
							temp.add(new BigDecimal(residuo));
							temp.add(monto.subtract(new BigDecimal(residuo)));
						}
					}
				}
			}
			salida.add(temp);
		});
		return salida.stream().filter(obj -> !obj.isEmpty()).distinct().collect(Collectors.toList());
	}

	private static boolean residuoInList(Double residuo) {
		BigDecimal parseResiduo = new BigDecimal(residuo);
		return denominacion.stream().anyMatch(p -> p.compareTo(parseResiduo) == 0);
	}

	private static boolean isEntero(Double resultado) {
		try {
			return resultado % 1 == 0;
		} catch (Exception e) {
			return false;
		}
	}
}
