package com.hatv.gestion.banco.controllers;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.hatv.gestion.banco.models.OrdenPago;
import com.hatv.gestion.banco.models.Sucursal;
import com.hatv.gestion.banco.services.IOrdenPagoService;
import com.hatv.gestion.banco.services.ISucursalService;

@Path("/sucursal")
@Produces(MediaType.APPLICATION_JSON)
public class SucursalController {

	@Autowired
	private ISucursalService sucursalService;

	@Autowired
	private IOrdenPagoService ordenPagoService;

	@GET
	@Path("/listar")
	public Response listar() {
		return Response.ok(sucursalService.listar()).build();
	}

	@POST
	@Path("/guardar")
	public Response guardar(Sucursal reqSucursal) {
		return Response.status(Status.CREATED).entity(sucursalService.guardar(reqSucursal)).build();
	}

	@PUT
	@Path("/modificar/{idSucursal}")
	public Response guardar(@PathParam("idSucursal") Long idSucursal, Sucursal reqSucursal) {
		return Response.status(Status.CREATED).entity(sucursalService.modificar(idSucursal, reqSucursal)).build();
	}

	@DELETE
	@Path("/eliminar/{idSucursal}")
	public Response eliminar(@PathParam("idSucursal") Long idSucursal) {
		sucursalService.eliminar(idSucursal);
		return Response.status(Status.NO_CONTENT).entity("").build();
	}

	@POST
	@Path("/procesadorOrden/{idSucursal}")
	public Response procesarOrden(@PathParam("idSucursal") Long idSucursal, OrdenPago reqOrdenPago) {
		OrdenPago response = ordenPagoService.procesarOrden(idSucursal, reqOrdenPago);
		return Response.status(Status.CREATED).entity(response).build();
	}

	@GET
	@Path("/listarPorSucursal/{idSucursal}")
	public Response listarPorSucursal(@PathParam("idSucursal") Long idSucursal, @QueryParam("moneda") String moneda) {
		return Response.status(Status.OK).entity(sucursalService.listarPorSucursal(idSucursal, moneda)).build();
	}
}
