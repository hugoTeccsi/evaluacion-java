package com.hatv.gestion.banco.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sucursales")
@XmlRootElement(name = "Sucursal")
public class Sucursal implements Serializable {

	private static final long serialVersionUID = -593124438163302443L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long idSucursal;
	private String nombre;
	private String direccion;
	@Column(name = "fecha_registro")
	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;

	@OneToMany(targetEntity = OrdenPago.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "sucursal_fk", referencedColumnName = "id")
	private List<OrdenPago> ordenesPago;
}
