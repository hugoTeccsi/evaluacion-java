package com.hatv.gestion.banco.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hatv.gestion.banco.models.OrdenPago;
import com.hatv.gestion.banco.models.Sucursal;
import com.hatv.gestion.banco.repository.IOrdenPagoRepository;
import com.hatv.gestion.banco.repository.ISucursalRepository;

@Service
public class OrdenPagoService implements IOrdenPagoService {

	@Autowired
	private IOrdenPagoRepository ordenPagoRepository;

	@Autowired
	private ISucursalRepository sucursalRepository;

	final static String ESTADO_ORDEN_ANULADA = "ANULADA";

	@Override
	public List<OrdenPago> listar() {
		return ordenPagoRepository.findAll();
	}

	@Override
	public OrdenPago guardar(OrdenPago ordenPago) {
		return ordenPagoRepository.save(ordenPago);
	}

	@Override
	public OrdenPago modificar(Long idOrdenPago, OrdenPago ordenPago) {
		OrdenPago obj = ordenPagoRepository.findById(idOrdenPago).get();
		obj.setMoneda(ordenPago.getMoneda());
		obj.setMonto(ordenPago.getMonto());
		obj.setEstado(ordenPago.getEstado());
		obj.setFechaPago(ordenPago.getFechaPago());
		return ordenPagoRepository.save(obj);
	}

	@Override
	public void eliminar(Long idOrdenPago) {
		OrdenPago obj = ordenPagoRepository.findById(idOrdenPago).get();
		obj.setEstado(ESTADO_ORDEN_ANULADA);
		ordenPagoRepository.save(obj);
	}

	@Override
	public OrdenPago procesarOrden(Long idSucursal, OrdenPago ordenPago) {
		OrdenPago obj = ordenPagoRepository.findById(ordenPago.getIdOrdenPago()).get();
		obj.setEstado(ordenPago.getEstado());
		obj.setFechaPago(new Date());

		Sucursal objSucursal = sucursalRepository.findById(idSucursal).get();
		List<OrdenPago> listOrdenPago = new ArrayList<>();
		listOrdenPago.add(obj);
		objSucursal.setOrdenesPago(listOrdenPago);
		sucursalRepository.save(objSucursal);

		return ordenPagoRepository.save(obj);
	}

}
